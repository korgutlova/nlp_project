import warnings

import pandas as pd
import json

import sys
from sklearn.externals import joblib
from sklearn.metrics import classification_report

from parsing import parse_command_line, check_file_exists


# Example command
#
# metrics.py --lm folder_for_model/classifier.pkl --src-test-texts biocreative6_dataset/data/gene_relations_in_sentences_test.json
#
from transformers.dep_features import DependencyFeatures


def get_analyze_errors(text, x):
    FX = 0
    result_new = []
    real_result = []
    for i in range(len(result)):
        if result[i] != custom_dataset.rel[i] and custom_dataset.rel[i] == x:
            if FX < 50:
                FX += 1
                result_new.append(result[i])
                real_result.append(custom_dataset.rel[i])
        else:
            result_new.append(result[i])
            real_result.append(custom_dataset.rel[i])
    print(">>Analyze error on 50 %s examples" % text)
    print(classification_report(real_result, result_new))


if __name__ == "__main__":

    warnings.simplefilter(action='ignore', category=FutureWarning)

    # -----------------------------------------------------------------------
    # READ PARAMETERS IN COMMAND LINE
    # -----------------------------------------------------------------------

    list_parameters = ["--lm", "--src-test-texts"]
    cust_dict = parse_command_line(sys.argv[1:], list_parameters)

    # -----------------------------------------------------------------------
    # LOAD TEST DATA
    # -----------------------------------------------------------------------

    print(">>Read data...")
    filename_clf = check_file_exists(cust_dict["--lm"])
    filename_test_data = check_file_exists(cust_dict["--src-test-texts"])

    with open(filename_test_data) as json_data:
        d = json.load(json_data)
    dataset = d["data"]
    custom_dataset = []

    print(">>Preprocessing...")
    for elem in dataset:
        for relation in elem["relations"]:
            relation_d = relation
            relation_d['text'] = elem["text"]
            custom_dataset.append(relation_d)
    custom_dataset = pd.DataFrame(custom_dataset)
    print(custom_dataset[:10])

    # -----------------------------------------------------------------------
    # LOAD MODEL
    # -----------------------------------------------------------------------
    print(">>Load model...")
    clf = joblib.load(filename_clf)

    part_of_filename = filename_test_data.split("/")
    dep_filename = part_of_filename[len(part_of_filename) - 1]
    if len(clf.named_steps["features"].get_params()["transformer_list"]) == 3:
        # Load dependencies dataset
        with open(
                'data/data_for_dependencies/dependency_%s' % dep_filename) as json_data:
            dependencies_dataset = json.load(json_data)
        # print(clf)
        clf.set_params(features=clf.named_steps['features'].set_params(dep_feats=DependencyFeatures(dependencies_dataset)))
    # print(clf)
    # -----------------------------------------------------------------------
    # PREDICTING
    # -----------------------------------------------------------------------
    print(">>Predict...")
    result = clf.predict(custom_dataset.drop(columns="rel", axis=1))
    # Get quality metrics of classification
    print(classification_report(custom_dataset.rel, result))

    # get_analyze_errors("false negative", 0)
    # get_analyze_errors("false positive", 1)
