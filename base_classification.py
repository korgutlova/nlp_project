import csv
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
from sklearn.feature_extraction.text import CountVectorizer, TfidfTransformer
from sklearn.multiclass import OneVsRestClassifier
from sklearn.pipeline import Pipeline, FeatureUnion
from sklearn.metrics import classification_report
from sklearn.preprocessing import FunctionTransformer


def reseiving_data(filename): #получение данных из файла
    data = {"word1": [], "word2": [], "relation": []}

    for name in filename:
        rdr = csv.DictReader(open(name, 'r'))
        for line in rdr:
            data.get("word1").append(line['word1'])
            data.get("word2").append(line['word2'])
            data.get("relation").append(line['rel'])

    return pd.DataFrame(data)

if __name__ == "__main__":

    data = reseiving_data(["biocreative6_dataset/data/gene_relations_baseline_train.csv"])
    print(data.head())
    print("dataframe added")

    # X_train, X_test, y_train, y_test = train_test_split(data.drop(['relation'], axis=1), data.relation, test_size=0.3, random_state=101)
    print("train data")
    get_word1_data = FunctionTransformer(lambda x: x['word1'], validate=False)
    get_word2_data = FunctionTransformer(lambda x: x['word2'], validate=False)

    process_and_join_features = Pipeline([
        ('features', FeatureUnion([
            ('text_features_1', Pipeline([
                ('selector', get_word1_data),
                ('vec', CountVectorizer())
            ])),
            ('text_features_2', Pipeline([
                ('selector', get_word2_data),
                ('vec', CountVectorizer())
            ]))
        ])),
        ('tfidf', TfidfTransformer()),
        ('clf', OneVsRestClassifier(LogisticRegression()))
    ])

    print("fit")
    process_and_join_features.fit(data.drop(['relation'], axis=1), data.relation)

    # ЗДЕСЬ ДОЛЖНО БЫТЬ СОХРАНЕНИЕ МОДЕЛИ
    # y = process_and_join_features.predict(X_test)
    # print("predict")
    # print(classification_report(y_test, y))
