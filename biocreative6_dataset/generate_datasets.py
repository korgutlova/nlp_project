import json

import pandas as pd

from help_methods import parsing_into_object


def add_relation(arr, entity, entity_two, offset1, offset2, rel):
    relation = {"word1": entity, "word2": entity_two, "offset1": offset1, "offset2": offset2, "rel": rel}
    arr.append(relation)
    return arr


def dump_to_json_file(data, filename):
    print("Generate file %s ..." % filename)
    data = {"data": data }
    f = open(filename, "w")
    json.dump(data, f, indent=2)
    f.close()

def add_relation_to_baseline(data_for_clf_baseline, entity, entity2, rel):
    data_for_clf_baseline["word1"].append(entity)
    data_for_clf_baseline["word2"].append(entity2)
    data_for_clf_baseline["rel"].append(rel)



if __name__ == "__main__":
    with open("data/PMtask_Relations_TrainingSet.json", encoding="UTF-8") as file:
        json_data = json.loads(file.read())
    json_data = json_data.get("documents")
    interactions = parsing_into_object(json_data)

    n = len(interactions)
    # n = 20
    data_for_clf_in = {}
    data_for_clf_out = []
    data_for_clf_baseline = {"word1": [], "word2": [], "rel": []}
    for i in range(n):
        entities_in_doc = set()
        unique_dict = set()
        texts = interactions[i]["text"]
        temp_out_dict = {"text": interactions[i]["full_text"], "relations": []}
        print(interactions[i]["entities"])
        for entity in interactions[i]["entities"]:
            for entity_two in interactions[i]["entities"]:
                offset_text = 0
                flag = False
                rel = 0
                offset1 = 0
                offset2 = 0
                delta = 0
                # проход по предложениям, с помощью offset смотрим в одном предложении слова или нет
                for k in range(len(texts)):
                    if k > 0:
                        offset_text += 1
                    temp_length = offset_text + len(texts[k])
                    for relation in interactions[i]["relations"]:
                        gene1 = relation.split(" ")[0]
                        gene2 = relation.split(" ")[1]
                        if (offset_text <= entity["offset"] < temp_length - len(entity["text"])
                                and offset_text <= entity_two["offset"] < temp_length - len(entity_two["text"])
                                and ((entity["offset"] + entity_two["offset"]) not in unique_dict and
                                     (entity_two["offset"] + entity["offset"]) not in unique_dict)
                                and entity["offset"] != entity_two["offset"]):
                            if (entity_two["NCBI GENE"] == gene2 and entity["NCBI GENE"] == gene1) \
                                    or (entity["NCBI GENE"] == gene2 and entity_two["NCBI GENE"] == gene1):

                                rel = 1
                            unique_dict.add(entity["offset"] + entity_two["offset"])
                            content = texts[k]
                            offset1 = entity["offset"] - offset_text
                            offset2 = entity_two["offset"] - offset_text
                            print("%s %s %s %s delta=%s" % (entity["text"], entity_two["text"], offset1,
                                                            offset2, delta))
                            if data_for_clf_in.get(content) is None:
                                data_for_clf_in[content] = []
                            add_relation(data_for_clf_in.get(content), entity["text"], entity_two["text"], offset1,
                                         offset2, rel)
                            add_relation_to_baseline(data_for_clf_baseline, entity["text"], entity_two["text"], rel)
                            flag = True
                    offset_text = temp_length
                rel = 0
                if not flag:
                    for relation in interactions[i]["relations"]:
                        gene1 = relation.split(" ")[0]
                        gene2 = relation.split(" ")[1]
                    if entity["offset"] != entity_two["offset"] and (
                            (entity["offset"] + entity_two["offset"]) not in unique_dict and
                            (entity_two["offset"] + entity["offset"]) not in unique_dict):
                        if (entity_two["NCBI GENE"] == gene2 and entity["NCBI GENE"] == gene1) \
                                or (entity["NCBI GENE"] == gene2 and entity_two["NCBI GENE"] == gene1):
                            rel = 1
                        unique_dict.add(entity["offset"] + entity_two["offset"])
                        add_relation(temp_out_dict["relations"], entity["text"], entity_two["text"], entity["offset"],
                                     entity_two["offset"], rel)
                        add_relation_to_baseline(data_for_clf_baseline, entity["text"], entity_two["text"], rel)
        data_for_clf_out.append(temp_out_dict)
    s1 = 0
    data_in = []
    for key in data_for_clf_in:
        inner_data = {"text": key, "relations": data_for_clf_in[key]}
        data_in.append(inner_data)
        print("Key %s" % key)
        s1 += 1
    s2 = 0
    for elem in data_for_clf_out:
        s2 += 1
    data_for_clf_baseline = pd.DataFrame(data_for_clf_baseline)
    print(s1)
    print(s2)
    print(len(data_for_clf_baseline))

#    train_size = 0.7
#    n_train_out = round(len(data_for_clf_out) * train_size)
#    print(
#        "Train data - %s and Test data %s relations out sentences" % (n_train_out, len(data_for_clf_out) - n_train_out))

#    n_train_in = round(len(data_for_clf_in) * train_size)
#    print(
#        "Train data - %s and Test data %s relations in sentences" % (n_train_in, len(data_in) - n_train_in))

#    n_train_baseline = round(len(data_for_clf_baseline) * train_size)
#    data_for_clf_baseline_train = data_for_clf_baseline[:n_train_baseline]
#    data_for_clf_baseline_test = data_for_clf_baseline[n_train_baseline:]

#    print("Generate file data/gene_relations_baseline_train.csv ...")
#    data_for_clf_baseline_train.to_csv("data/gene_relations_baseline_train.csv", sep=',', encoding='utf-8')
#    print("Generate file data/gene_relations_baseline_test.csv ...")
#    data_for_clf_baseline_test.to_csv("data/gene_relations_baseline_test.csv", sep=',', encoding='utf-8')

#    dump_to_json_file(data_in[:n_train_in], "data/gene_relations_in_sentences_train.json")
#    dump_to_json_file(data_in[n_train_in:], "data/gene_relations_in_sentences_test.json")
#    dump_to_json_file(data_for_clf_out[:n_train_out], "data/gene_relations_out_sentences_train.json")
#    dump_to_json_file(data_for_clf_out[n_train_out:], "data/gene_relations_out_sentences_test.json")

    dump_to_json_file(data_in, "data/gene_relations_in_sentences.json")
    dump_to_json_file(data_for_clf_out, "data/gene_relations_out_sentences.json")
