import nltk


def print_data(a):
    print("Sentences: ")
    print(a["text"])
    print("Entities:")
    for k in a["entities"]:
        print("[text=\"%s\" NCBI GENE=%s length=%s offset=%s]" %
              (k["text"], k["NCBI GENE"], k["length"], k["offset"]))
    print("")
    print("Relations:")
    for k in a["relations"]:
        print("[%s]" % k)
    print("")
    print("")


def add_entities(new_data, smt_data):
    # шаблон entity
    for a in smt_data["annotations"]:
        entity = {"text": a["text"], "NCBI GENE": a["infons"]["NCBI GENE"], "length": a["locations"][0]["length"],
                  "offset": a["locations"][0]["offset"]}
        new_data["entities"].append(entity)
    return new_data


def parsing_into_object(json_data):
    new_interactions = []
    for item in json_data:
        new_data = {"full_text" : [], "text": [], "relations": [], "entities": []}

        title_data = item.get("passages")[0]
        text_data = item.get("passages")[1]
        hybrid_text = title_data.get("text") + " " + text_data.get("text")
        new_data["text"] = nltk.sent_tokenize(hybrid_text)
        new_data["full_text"] = hybrid_text
        new_data = add_entities(new_data, text_data)
        new_data = add_entities(new_data, title_data)
        [new_data["relations"].append("%s %s" % (a["infons"]["Gene1"], a["infons"]["Gene2"])) for a in
         item["relations"]]
        new_interactions.append(new_data)
    return new_interactions