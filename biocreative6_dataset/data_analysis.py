import csv
import json

import pandas as pd

from help_methods import parsing_into_object


def check_between_sentence(entity, entity_two, offset_text, temp_length, param):
    return (offset_text <= entity["offset"] < temp_length <= entity_two["offset"] < temp_length + param
            or offset_text <= entity_two["offset"] < temp_length <= entity["offset"] < temp_length + param)


def add_entity_in_relations(entities_in_doc, entities, smt_entity):
    if smt_entity["offset"] not in entities_in_doc:
        entities_in_doc.add(smt_entity["offset"])
        entities.append(smt_entity["NCBI GENE"])
    return entities_in_doc, entities


if __name__ == "__main__":
    with open("data/PMtask_Relations_TrainingSet.json", encoding="UTF-8") as file:
        json_data = json.loads(file.read())
    json_data = json_data.get("documents")
    interactions = parsing_into_object(json_data)

    sum_relations = 0
    entities = []
    sum_relations_into = 0
    sum_relations_between = 0
    n = len(interactions)
    # frames = []
    # n = 20
    for i in range(n):
        # подсчет количества отношений между и внутри предложений
        entities_in_doc = set()
        for relation in interactions[i]["relations"]:
            gene1 = relation.split(" ")[0]
            gene2 = relation.split(" ")[1]
            unique_dict = set()
            # data_for_clf = {"word1": [], "word2": [], "rel": []}
            for entity in interactions[i]["entities"]:
                for entity_two in interactions[i]["entities"]:
                    rel = 0
                    if (entity_two["NCBI GENE"] == gene2 and entity["offset"] != entity_two["offset"] and
                            entity["NCBI GENE"] == gene1 and entity["offset"] not in unique_dict):
                        offset_text = 0
                        texts = interactions[i]["text"]
                        flag = False
                        rel = 1
                        # проход по предложениям, с помощью offset смотрим в одном предложении слова или нет
                        for k in range(len(texts)):
                            temp_length = offset_text + len(texts[k])
                            if (offset_text <= entity["offset"] < temp_length
                                    and offset_text <= entity_two["offset"] < temp_length):
                                sum_relations_into += 1
                                unique_dict.add(entity_two["offset"])

                                flag = True
                                break
                            offset_text = temp_length

                        if not flag:
                            sum_relations_between += 1
                        entities_in_doc, entities = add_entity_in_relations(entities_in_doc, entities,
                                                                            entity)
                        entities_in_doc, entities = add_entity_in_relations(entities_in_doc, entities,
                                                                            entity_two)
                    flag = True
                    # for s in range(len(data_for_clf["word1"])):
                    #     if ((data_for_clf["word1"][s] == entity["text"] and data_for_clf["word2"][s] == entity_two[
                    #         "text"])
                    #             or (data_for_clf["word1"][s] == entity_two["text"] and data_for_clf["word2"][s] ==
                    #                 entity["text"])):
                    #         if data_for_clf["rel"][s] == 0:
                    #             data_for_clf["rel"][s] = rel
                    #         flag = False
                    #         break
                    # if flag:
                    #     data_for_clf["word1"].append(entity["text"])
                    #     data_for_clf["word2"].append(entity_two["text"])
                    #     data_for_clf["rel"].append(rel)

            # frames.append(pd.DataFrame(data_for_clf))

    unique_entities = set(entities)
    # result = pd.concat(frames).reset_index(drop=True)
    # result.to_csv("gene_relations.csv", sep=',', encoding='utf-8')

    print("Number of Documents - %s " % len(interactions))
    print("Number of all Relations - %s " % (sum_relations_into + sum_relations_between))
    print("Number of relations in the sentence - %s " % sum_relations_into)
    print("Number of relations between sentences - %s " % sum_relations_between)
    print("Number of all Entities - %s " % len(entities))
    print("Number of Unique Entities - %s " % len(unique_entities))
