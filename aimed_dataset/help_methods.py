import os, re, sys, io
import nltk
import json

def read_corpus_data(data_path):
	doc_files = os.listdir(data_path)

	corpus = []
	for filename in doc_files:
		with open(data_path + filename, 'r') as file:
			doc = file.read().replace('\n', ' ')
			doc = re.sub(" {2,}" , " ", doc)
			doc = nltk.sent_tokenize(doc)
			corpus.append(doc)

	return corpus

def dump_to_json_file(data, filename):

	with io.open(filename + ".json", 'w', encoding='utf8') as json_file:
		json_file.write(json.dumps(data, indent=2))


if __name__ == "__main__":
	pass