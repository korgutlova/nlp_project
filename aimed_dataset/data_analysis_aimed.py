import os, re, sys
import nltk
from collections import namedtuple

Protein = namedtuple("Protein", "name rel_id sentence_num")
Relation = namedtuple("Relation", "protein1 protein2")

#--src-train-texts
PATH = "data/aimed_interactions/"

DOC_COUNT = 0
RELATION_COUNT_ALL = 0
RELATION_COUNT_WITHIN_SENT = 0
ENTITIES_COUNT = 0
UNIQ_ENTITIES_COUNT = 0
ENTITIES_COUNT_IN_REL = 0
UNIQ_ENTITIES_COUNT_IN_REL = 0

entities_set = set()
entities_in_rel_set = set()

doc_files = os.listdir(PATH)
DOC_COUNT = len(doc_files)

#read data
corpus = []
for filename in doc_files:
	with open(PATH + filename, 'r') as file:
		data = file.read().replace('\n', ' ')
		corpus.append(data)


for doc in corpus:

	#remove extra spaces
	doc = re.sub(" {2,}" , " ", doc)
	doc = nltk.sent_tokenize(doc)

	proteins_in_rel = []

	for sent_i in range(len(doc)):

		#find tagget words
		tags_pattern = re.compile(r'(?:<p[12] pair=[0-9]+ > )*<prot>[^<]+</prot> (?:</p[12]> )*')
		entities_in_tags = tags_pattern.findall(doc[sent_i])
		ENTITIES_COUNT += 1

		for entity_in_tag in entities_in_tags:

			#find tagget words
			match_obj = re.search(r'<prot> (?P<entity>[^<]+) </prot>', entity_in_tag)
			
			#extraction entity
			entity = match_obj.group('entity')
			entities_set.add(entity)

			#extraction relatons
			rels= re.findall(r'<p(?P<prot_i>[12]) pair=(?P<rel_id>[0-9])+ >', entity_in_tag)
			for rel in rels:
				prot_i, rel_id = int(rel[0]), int(rel[1])
				ENTITIES_COUNT_IN_REL += 1
				entities_in_rel_set.add(entity)

				protein = Protein(name=entity, rel_id=rel_id, sentence_num=sent_i)

				if (prot_i == 1):
					proteins_in_rel.append(protein)
				else:
					protein2 = protein

					for protein1 in proteins_in_rel:
						if protein1.rel_id == protein2.rel_id:
							relation = Relation(protein1=protein1, protein2=protein2)
							if protein1.sentence_num == protein2.sentence_num:
								RELATION_COUNT_WITHIN_SENT += 1
							proteins_in_rel.remove(protein1)

							print("RELATION: <%s> AND <%s>" %(protein1.name, protein2.name) )
							break

					RELATION_COUNT_ALL += 1;

UNIQ_ENTITIES_COUNT = len(entities_set)
UNIQ_ENTITIES_COUNT_IN_REL = len(entities_in_rel_set)



print (	"-" * 18 + "STATICTICS" + "-" * 26 + "\n\
		DOC COUNT: %d\n\
		RELATION COUNT (ALL): %d\n\
		RELATION COUNT (WITHIN SENT): %d\n\
		ENTITIES COUNT: %d\n\
		UNIQ ENTITIES COUNT: %d\n\
		ENTITIES COUNT IN REL: %d\n\
		UNIQ ENTITIES COUNT IN REL: %d\n"

		%(DOC_COUNT,\
		RELATION_COUNT_ALL,\
		RELATION_COUNT_WITHIN_SENT,\
		ENTITIES_COUNT,\
		UNIQ_ENTITIES_COUNT,\
		ENTITIES_COUNT_IN_REL,\
		UNIQ_ENTITIES_COUNT_IN_REL)\
		+ "-" * 54
	)