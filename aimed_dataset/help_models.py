class Protein():
	def __init__(self, name, id, sentence_id, sent_offset, doc_offset):
		self.name = name
		self.id = id
		self.sentence_id = sentence_id
		self.sent_offset = sent_offset
		self.doc_offset = doc_offset
		self.rels_id = []

	def append_rel(self, rel_id):
		self.rels_id.append(rel_id)

	def is_in_rel_with(self, entity):
		for self_rel_id in self.rels_id:
			for entity_rel_id in entity.rels_id:
				if self_rel_id == entity_rel_id:
					return 1
		return 0

	def __str__(self):
		return 'ENTITY: name - %s, id - %s, rels_id - %s)' \
			%(self.name, str(self.id), str(self.rels_id))

if __name__ == "__main__":
	pass