import re
import json

from help_models import Protein
from help_methods import read_corpus_data
from help_methods import dump_to_json_file

#--src-train-texts
PATH = "data/aimed_interactions/"

if __name__ == "__main__":

	#read data-----------------------------------
	corpus = read_corpus_data(PATH)
	#--------------------------------------------

	#data for output json
	relations_in_sentences = []
	relations_out_sentences = []

	entities_id_counter = 0

	positive = 0
	negative = 0

	for doc in corpus:
		
		doc_data = {}
		doc_entities = [] #list of entities objects in this doc
		sent_offset = 0


		for sent_i in range(len(doc)):

			print(doc[sent_i])
			sent_data = {}
			sent_entities = [] #list of entities objects in this sentence 
			
			#find tagget enitities-------------------------------------------------------------------
#			tags_pattern = re.compile(r'(?:<p[12] pair=[0-9]+ > )*<prot>[^<]+</prot> (?:</p[12]> )*')
			tags_pattern = re.compile(r'(?:<p[12] pair=[0-9]+ > )*(<prot>[^<]* ?){1,2}([^<]* ?</prot>){1,2} (?:</p[12]> )*')
			tag_match_obj = tags_pattern.search(doc[sent_i])

			while tag_match_obj is not None:
				
				entities_id_counter += 1

				entity_in_tag = tag_match_obj.group(0)
				entity_offset = tag_match_obj.start(0)

				#extraction entity
				entity = re.search(r'<prot> (?P<entity>[^<]+) ?</prot>', entity_in_tag).group('entity').rstrip()
#				entity = re.search(r'(<prot>[^<]* ?){1,2}([^<]* ?</prot>){1,2}', entity_in_tag).group(0).replace('</prot>', '').replace('<prot>', '').lstrip().rstrip()

				#remove tags from text
				doc[sent_i] = tags_pattern.sub(entity + ' ', doc[sent_i], count=1)

				protein = Protein(	name=entity, id=entities_id_counter, sentence_id=sent_i,\
									sent_offset=entity_offset, doc_offset=sent_offset+entity_offset)

				#extraction relatons------------------------------------------------------------
				rels= re.findall(r'<p(?P<prot_i>[12]) pair=(?P<rel_id>[0-9])+ >', entity_in_tag)
				for rel in rels:
					prot_i, rel_id = int(rel[0]), int(rel[1])
					protein.append_rel(rel_id)

				sent_entities.append(protein)

				#find next tagget enitity
				tag_match_obj = tags_pattern.search(doc[sent_i])
			#-------------------------------------------------------------------------------------------

			sent_offset += len(doc[sent_i]) + 1
			doc_entities.extend(sent_entities)

			#build relations in sentence data-----------------------------------------------------------
			sent_data['text'] = doc[sent_i]
			sent_data['relations'] = []

			for entity1_i  in range(len(sent_entities)):
				for entity2_i  in range(entity1_i+1, len(sent_entities)):

					entity1 = sent_entities[entity1_i]
					entity2 = sent_entities[entity2_i]


					if entity1.is_in_rel_with(entity2):
						positive += 1
					else:
						negative += 1	

					relation = {}
					relation["word1"] = entity1.name
					relation["word2"] = entity2.name
					relation["rel"] = entity1.is_in_rel_with(entity2)
					relation["offset1"] = entity1.sent_offset
					relation["offset2"] = entity2.sent_offset

					sent_data['relations'].append(relation)
					if entity1.is_in_rel_with(entity2):
						print(relation)

			if len(sent_data['relations']) > 0:
				relations_in_sentences.append(sent_data)
			#-------------------------------------------------------------------------------------------

		
		#build data out sentences-------------------------------------
		doc_data['text'] = ' '.join(doc)
		doc_data['relations'] = []


		for entity1_i  in range(len(doc_entities)):
			for entity2_i  in range(entity1_i+1, len(doc_entities)):

				entity1 = doc_entities[entity1_i]
				entity2 = doc_entities[entity2_i]

				if(entity1.sentence_id != entity2.sentence_id):
					relation = {}
					relation["word1"] = entity1.name
					relation["word2"] = entity2.name
					relation["rel"] = entity1.is_in_rel_with(entity2)				
					relation["offset1"] = entity1.doc_offset
					relation["offset2"] = entity2.doc_offset

					print(relation)
					doc_data['relations'].append(relation)

		relations_out_sentences.append(doc_data)
		#--------------------------------------------------------------

	#write data------------------------------------------

#	train_size = 0.7
#	n_train_out = round(len(relations_out_sentences) * train_size)
#	n_train_in = round(len(relations_in_sentences) * train_size)
#	data_out_train = {"data": relations_out_sentences[:n_train_out]}
#	data_out_test = {"data": relations_out_sentences[n_train_out:]}
#	data_in_train = {"data": relations_in_sentences[:n_train_in]}
#	data_in_test = {"data": relations_in_sentences[n_train_in:]}
#	dump_to_json_file(data_out_train, "data/relations_out_sentences_train")
#	dump_to_json_file(data_out_test, "data/relations_out_sentences_test")
#	dump_to_json_file(data_in_train, "data/relations_in_sentences_train")
#	dump_to_json_file(data_in_test, "data/relations_in_sentences_test")

	print(positive)
	print(negative)
	
	data_out = {"data": relations_out_sentences}
	data_in = {"data": relations_in_sentences}

	dump_to_json_file(data_in, "data/relations_in_sentences")
	dump_to_json_file(data_out, "data/relations_out_sentences")