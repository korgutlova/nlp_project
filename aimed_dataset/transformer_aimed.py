import os, re, sys
import nltk
import pandas as pd

from aimed_dataset.help_models import Protein
from aimed_dataset.help_methods import read_corpus_data


#--src-train-texts
PATH = "data/aimed_interactions/"



if __name__ == "__main__":

	#read data-----------------------------------
	corpus = read_corpus_data(PATH)
	#--------------------------------------------

	data_for_clf = {"word1": [], "word2": [], "rel": []}
	entities_id_counter = 0

	for doc in corpus:

		entities = []

		#remove extra spaces
		doc = re.sub(" {2,}" , " ", doc)
		doc = nltk.sent_tokenize(doc)


		for sent_i in range(len(doc)):

			#find tagget words
			tags_pattern = re.compile(r'(?:<p[12] pair=[0-9]+ > )*<prot>[^<]+</prot> (?:</p[12]> )*')
			entities_in_tags = tags_pattern.findall(doc[sent_i])

			for entity_in_tag in entities_in_tags:

				entities_id_counter += 1

				#find tagget words
				match_obj = re.search(r'<prot> (?P<entity>[^<]+) </prot>', entity_in_tag)
				
				#extraction entity
				entity = match_obj.group('entity')
				protein = Protein(name=entity, id=entities_id_counter, sentence_id=sent_i)

				#extraction relatons
				rels= re.findall(r'<p(?P<prot_i>[12]) pair=(?P<rel_id>[0-9])+ >', entity_in_tag)
				for rel in rels:
					prot_i, rel_id = int(rel[0]), int(rel[1])

					protein.append_rel(rel_id)

				entities.append(protein)

		#build data for clf
		for entity1_i  in range(len(entities)):
			for entity2_i  in range(entity1_i+1, len(entities)):

				entity1 = entities[entity1_i]
				entity2 = entities[entity2_i]
				rel = entity1.is_in_rel_with(entity2)

				data_for_clf["word1"].append(entity1.name)
				data_for_clf["word2"].append(entity2.name)
				data_for_clf["rel"].append(rel)

	#write data
	# train_size = 0.7
	output_data = pd.DataFrame(data_for_clf)
	# n_train = round(len(output_data) * train_size)
	# output_data_train = output_data[:n_train]
	# output_data_test = output_data[n_train:]
	# output_data_train.to_csv("data/aimed_data_baseline_train.csv", sep=',', encoding='utf-8')
	# output_data_test.to_csv("data/aimed_data_baseline_test.csv", sep=',', encoding='utf-8')
	output_data.to_csv("data/aimed_data_baseline.csv", sep=',', encoding='utf-8')