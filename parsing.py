import sys
from pathlib import Path


def parse_command_line(args, list_param):
    custom_dict = {}
    i = 0
    j = 0
    if len(args) / 2 != len(list_param):
        print("Неверное количество параметров")
        sys.exit(1)
    while i < len(args):
        if list_param[j] != args[i]:
            raise Exception("Неверный параметер %s" % args[i])
        custom_dict[args[i]] = args[i + 1]
        i += 2
        j += 1
    return custom_dict


def check_file_exists(filename):
    custom_file = Path(filename)
    if not custom_file.exists():
        print("Файла %s не существует!" % filename)
        sys.exit(1)
    return filename
