import gensim
import sys
import json

DS_FILENAME = 'aimed_dataset/data/relations_in_sentences.json'
WORD2VEC_FILENAME = 'data/word2vec_model'

if (len(sys.argv) > 1 and sys.argv[1] == 'train'):
    # read data
    data = []
    print(">>Read data...")
    with open(DS_FILENAME, 'r') as f:
        data = json.loads(f.read())['data']

    print(">>Preproccess data...")
    for i in range(len(data)):
        data[i] = gensim.utils.simple_preprocess(data[i]['text'])

    # build vocabulary and train model
    print(">>Train model...")
    model = gensim.models.Word2Vec(
        data,
        size=150,
        window=10,
        min_count=2,
        workers=5)

    model.train(data, total_examples=len(data), epochs=10)

    # save model
    model.save(WORD2VEC_FILENAME)
#--------------------------------------------------------------
# load model
model = gensim.models.Word2Vec.load(WORD2VEC_FILENAME)

index2word_set = set(model.wv.vocab)
print(index2word_set)