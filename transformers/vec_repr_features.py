import numpy as np
import nltk
import gensim
import pandas as pd
from sklearn.base import BaseEstimator, TransformerMixin


WORD2VEC_FILE_NAME = 'data/PubMed-and-PMC-w2v.bin'


class Word2VecFeature(BaseEstimator, TransformerMixin):

    model = None
    index2word_set = None
    num_features = 200
    
    def __init__(self):
        if Word2VecFeature.model == None:
            print(">>Load word2vec model...")
            Word2VecFeature.model = gensim.models.KeyedVectors.load_word2vec_format(WORD2VEC_FILE_NAME, binary=True)
            print(">>Creating word2vec vocab...")
            Word2VecFeature.index2word_set = set(Word2VecFeature.model.wv.vocab)


    def get_feature_names(self):
        feature_names = []
        for i in range(Word2VecFeature.num_features):
            feature_names.append('word1_feat%s' %i)
        for i in range(Word2VecFeature.num_features):
            feature_names.append('word2_feat%s' %i)
        return feature_names


    def fit(self, x, y=None):
        return self


    def transform(self, X):
        features = []
        for rel in X.to_dict('records'):
            rel_feat_entity1 = self.makeEntityVec(nltk.word_tokenize(rel['word1']))
            rel_feat_entity2 = self.makeEntityVec(nltk.word_tokenize(rel['word2']))
            features.append(np.append(rel_feat_entity1, rel_feat_entity2))
        return features


    def makeEntityVec(self, entity):

        featureVec = np.zeros((Word2VecFeature.num_features,),dtype="float32")
        nwords = 0.

        for word in entity:
            if word in Word2VecFeature.index2word_set:
                nwords += 1.
                featureVec = np.add(featureVec, Word2VecFeature.model[word])

        if nwords > 1:
            featureVec = np.divide(featureVec,nwords)
        return featureVec