# BASE FEATURES
# WORD1: first entity text +
# WORD2: second entity text +


from sklearn.feature_extraction.text import CountVectorizer
from sklearn.base import BaseEstimator, TransformerMixin

import numpy as np


class BaseFeatures(BaseEstimator, TransformerMixin):

    base_feats = ["WORD1", "WORD2"]

    def __init__(self):
        self.count_vect = CountVectorizer()


    def get_feature_names(self):
        feature_names = []
        for base_feat in BaseFeatures.base_feats:
            feature_names.extend([base_feat + '_' + feat_name for feat_name in 
                self.count_vect.get_feature_names()])
        return feature_names


    def fit(self, X, y=None):
        
        features = BaseFeatures.get_features(X)
        self.count_vect.fit(np.concatenate([features["WORD1"], features["WORD1"]]))
        return self


    def transform(self, X):

        transform_feats = []
        features = BaseFeatures.get_features(X)

        transform_feats.append(self.count_vect.transform(features["WORD1"]).toarray())
        transform_feats.append(self.count_vect.transform(features["WORD2"]).toarray())

        transform_feats = np.concatenate(transform_feats, axis=1)
        return transform_feats

    def get_features(X):

        features = {"WORD1": [], "WORD2": []}

        for relation in X.to_dict('records'):
            features["WORD1"].append(relation["word1"])
            features["WORD2"].append(relation["word2"])
      
        return features