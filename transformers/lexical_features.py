# DMTXT - WORD1 +
# CMTXT - WORD2 +

# WVNULL: when no verb in between +
# WVFL: when only one verb in between +
# WVTFL: the verb when only one verb in between +
# WVF: first verb in between when at least two verbs in between +
# WVL: last verb in between when at least two verbs in between +
# WBNULL: when no word in between +
# WBFL: when only one word in between +
# WBTFL: the word when only one word in between +
# WBTPOS: part of speech of the word in between when only one word in between +
# WBF: first word in between when at least two words in between +
# WBFPOS: part of speech of the first word in between when at least two words in between +
# WBL: last word in between when at least two words in between +
# WBLPOS: part of speech of the last word in between when at least two words in between +

import string

import nltk
from nltk import word_tokenize, re

from sklearn.feature_extraction.text import CountVectorizer
from sklearn.base import BaseEstimator, TransformerMixin

import numpy as np


class LexicalFeatures(BaseEstimator, TransformerMixin):

    text_feats = ["WORD1", "WORD2", "WVTFL", "WVF", "WVL", "WBTFL", "WBF", "WBL"]
    numeric_feats = ['WVNULL', 'WBNULL', "WVFL", "WBFL", "WBTPOS", "WBFPOS", "WBLPOS"]

    def __init__(self):
        self.count_vects = {text_feat: CountVectorizer() for text_feat in LexicalFeatures.text_feats}
        self.feats_data = LexicalFeatures.dataset_init()
        self.pos_dict = LexicalFeatures.init_part_of_speech()
        self.feats_computing = False


    def get_feature_names(self):
        feature_names = []
        feature_names.extend(LexicalFeatures.numeric_feats)
        for text_feat in LexicalFeatures.text_feats:
           feature_names.extend([text_feat + '_' + feat_name for feat_name in 
                self.count_vects[text_feat].get_feature_names()])
        return feature_names


    def fit(self, X, y=None):
        
        self.compute_lexical_features(X)

        for text_feat in LexicalFeatures.text_feats:
            self.count_vects[text_feat].fit(self.feats_data[text_feat])

        return self


    def transform(self, X):

        features = []

        self.feats_data = LexicalFeatures.dataset_init()
        self.compute_lexical_features(X)

        for num_feat in LexicalFeatures.numeric_feats:
            features.append(np.reshape(self.feats_data[num_feat], (-1,1)))

        for text_feat in LexicalFeatures.text_feats:
            features.append(self.count_vects[text_feat].transform(self.feats_data[text_feat]).toarray())

        features = np.concatenate(features, axis=1)

        return features


    def init_part_of_speech():
        part_dict = {}
        index = 1
        for line in open("data/part_of_speech.txt", "r"):
            part_dict[line.split(" ")[0]] = index
            index += 1
        print(part_dict)
        return part_dict

    def compute_lexical_features(self, X):

        for relation in X.to_dict('records'):
            is_verb = lambda pos: pos[:2] == 'VB'
            # необходимые признаки
            wvnull = 1
            wvfl = 0
            wbnull = 1
            wbfl = 0
            wvtfl = ""
            wvf = ""
            wvl = ""
            wbtfl = ""
            wbtpos = 0
            wbf = ""
            wbl = ""
            wbfpos = 0
            wblpos = 0
            # сколько токенов до 1 gene
            before_elem_n = len(word_tokenize(relation['text'][:relation["offset1"] + len(relation["word1"])]))
            # сколько токенов после 2 gene
            after_elem_n = len(word_tokenize(relation['text'][relation["offset2"] - 1:]))
            # все токены
            tokens = word_tokenize(relation['text'])
            tokens_with_pos = []
            # определение частей речи
            for (word, pos) in nltk.pos_tag(tokens[before_elem_n:len(tokens) - after_elem_n]):
                tokens_with_pos.append([word, pos])
            # очищение пунктуации
            clear_tokens = []
            for token in tokens_with_pos:
                if token[0] not in string.punctuation:
                    clear_tokens.append(token)
    #        print(clear_tokens)
            if len(tokens) != 0:
                if len(clear_tokens) > 0:
                    wbnull = 0
                if len(clear_tokens) == 1:
                    wbfl = 1
                    wbtfl = clear_tokens[0][0]
                    wbtpos = self.pos_dict[clear_tokens[0][1]]
                if len(clear_tokens) > 1:
                    wbf = clear_tokens[0][0]
                    wbl = clear_tokens[len(clear_tokens) - 1][0]
                    wbfpos = self.pos_dict[clear_tokens[0][1]]
                    wblpos = self.pos_dict[clear_tokens[len(clear_tokens) - 1][1]]
                verbs = []
                for elem in clear_tokens:
                    if is_verb(elem[1]):
                        verbs.append(elem[0])
                if len(verbs) > 0:
                    wvnull = 0
                if len(verbs) == 1:
                    wvfl = 1
                    wvtfl = verbs[0]
                if len(verbs) > 1:
                    wvf = verbs[0]
                    wvl = verbs[len(verbs) - 1]

            self.feats_data["WORD1"].append(relation["word1"])
            self.feats_data["WORD2"].append(relation["word2"])
            self.feats_data["WVNULL"].append(wvnull)
            self.feats_data["WVFL"].append(wvfl)
            self.feats_data["WVTFL"].append(wvtfl)
            self.feats_data["WVF"].append(wvf)
            self.feats_data["WVL"].append(wvl)
            self.feats_data["WBNULL"].append(wbnull)
            self.feats_data["WBFL"].append(wbfl)
            self.feats_data["WBTFL"].append(wbtfl)
            self.feats_data["WBTPOS"].append(wbtpos)
            self.feats_data["WBF"].append(wbf)
            self.feats_data["WBL"].append(wbl)
            self.feats_data["WBFPOS"].append(wbfpos)
            self.feats_data["WBLPOS"].append(wblpos)

        self.feats_computing = True

    def dataset_init():
        return {"WORD1": [], "WORD2": [], "WVNULL": [],
                "WVFL": [], "WBNULL": [], "WBFL": [], "WVTFL": [],
                "WVF": [], "WVL": [], "WBTFL": [], "WBTPOS": [],
                "WBF": [], "WBL": [], "WBFPOS": [], "WBLPOS": []}