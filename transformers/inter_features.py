# CPOS: part of speech of chemical mention - word 1 +
# DPOS: part of speech of disease mention - word 2 +
# SDIST: sentence distance between chemical mention and disease mention (max distance) +
# CFRQ: word1 frequency in document +
# DFRQ: word2 frequency in document +

import nltk
import numpy as np
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.feature_extraction.text import CountVectorizer


class InterSentenceFeatures(BaseEstimator, TransformerMixin):
    text_feats = ["word1", "word2"]
    numeric_feats = ["SDIST", "CPOS", "DPOS", "CFRQ", "DFRQ"]

    def __init__(self):
        self.count_vects = {text_feat: CountVectorizer() for text_feat in InterSentenceFeatures.text_feats}
        self.pos_dict = InterSentenceFeatures.init_part_of_speech()
        self.feats_data = {}
        self.feats_computing = False

    def get_feature_names(self):
        feature_names = []
        feature_names.extend(InterSentenceFeatures.numeric_feats)
        for text_feat in InterSentenceFeatures.text_feats:
            feature_names.extend([text_feat + '_' + feat_name for feat_name in
                                  self.count_vects[text_feat].get_feature_names()])
        return feature_names

    def fit(self, X, y=None):
        self.compute_inter_sentence_features(X)

        for text_feat in InterSentenceFeatures.text_feats:
            self.count_vects[text_feat].fit(self.feats_data[text_feat])

        return self

    def transform(self, X):
        features = []

        self.feats_data = X

        self.compute_inter_sentence_features(X)

        for num_feat in InterSentenceFeatures.numeric_feats:
            features.append(np.reshape(self.feats_data[num_feat], (-1, 1)))

        for text_feat in InterSentenceFeatures.text_feats:
            features.append(self.count_vects[text_feat].transform(self.feats_data[text_feat]).toarray())

        features = np.concatenate(features, axis=1)

        return features

    def init_part_of_speech():
        part_dict = {}
        index = 1
        for line in open("data/part_of_speech.txt", "r"):
            part_dict[line.split(" ")[0]] = index
            index += 1
        return part_dict

    def compute_inter_sentence_features(self, X):
        X = X.reset_index()
        self.feats_data = X.reset_index()
        print('add data')
        self.feats_data['SDIST'] = self.sdist(X)
        print('add SDIST')
        self.feats_data['CPOS'] = self.cdpos(X, 'word1')
        print('add CPOS')
        self.feats_data['DPOS'] = self.cdpos(X, 'word2')
        print('add DPOS')
        self.feats_data['CFRQ'] = self.cdfrq(X, 'word1')
        print('add CFRQ')
        self.feats_data['DFRQ'] = self.cdfrq(X, 'word2')
        print('add DFRQ')
        self.feats_computing = True

    def cdpos(self, data, wordname):
        res = []
        tokenize = nltk.RegexpTokenizer(r'\w+')
        for i in range(len(data)):
            word = data[wordname][i].lower()
            word = tokenize.tokenize(word)
            word = nltk.Text(word)
            word = nltk.pos_tag(word)
            inp = ''
            for item in word:
                if item[1] in self.pos_dict.keys():
                    inp = inp + str(self.pos_dict[item[1]])
            res.append(int(inp))

        return res

    def cdfrq(self, data, wordname):
        res = []
        tokenize = nltk.RegexpTokenizer(r'\w+')
        for i in range(len(data)):
            text = data['text'][i].lower()
            word = data[wordname][i].lower()
            m = text.count(word)
            res.append(m * len(tokenize.tokenize(word)) / len(tokenize.tokenize(text)))

        return res

    def sdist(self, data):
        res = []
        for i in range(len(data)):
            sentences = nltk.sent_tokenize(data['text'][i])
            index = 0
            j = 0
            dist = 0
            offset1 = min(data['offset1'][i], data['offset2'][i])
            offset2 = max(data['offset2'][i], data['offset1'][i])
            while (index < offset1):
                index += len(sentences[j]) + 1
                j += 1
            while (index < offset2):
                index += len(sentences[j]) + 1
                j += 1
                dist += 1
            res.append(dist)
        return res
