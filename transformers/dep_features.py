# DEPENDENCY FEATURES
# WCDD: whether chemical mention and disease mention connect directly +
# WRCD: whether root and chemical mention connect directly +
# WRDD: whether root and disease mention connect directly +


import json
import nltk
import pandas as pd
import numpy as np
from sklearn.base import BaseEstimator, TransformerMixin


class DependencyFeatures(BaseEstimator, TransformerMixin):
    dep_feats_names = ["WCDD", "WRCD", "WRDD"]

    def __init__(self, dep_dataset):
        self.dep_dataset = dep_dataset

    def get_feature_names(self):
        return self.dep_feats_names

    def fit(self, X, y=None):
        return self

    def transform(self, X):

        features = []
        feats_dict = self.compute_dep_feats(X)

        for dep_feat in DependencyFeatures.dep_feats_names:
            features.append(np.reshape(feats_dict[dep_feat], (-1, 1)))

        features = np.concatenate(features, axis=1)

        return features

    def get_indexes_gene(sentence, offset, word):
        before_gene_n = len(nltk.word_tokenize(sentence[:offset]))
        gene_len = len(nltk.word_tokenize(word))

        array_gene = []
        while gene_len != 0:
            array_gene.append(before_gene_n)
            before_gene_n += 1
            gene_len -= 1
        return array_gene

    def check(dep_root, dep_one, dep_two, array_gene, x):
        if (dep_root == dep_one and dep_two in array_gene) or \
                (dep_root == dep_two and dep_one in array_gene):
            return 1
        else:
            return x

    def compute_dep_feats(self, X):

        dep_feats = {"WCDD": [], "WRCD": [], "WRDD": []}

        i = 0
        X = X.reset_index()
        current_text = X["text"][0]
        # print(current_text)
        dependencies = self.dep_dataset[0]
        for relation in X.to_dict('records'):
            if current_text != relation["text"]:
                current_text = relation["text"]
                dependencies = self.dep_dataset[i]
                i += 1
            # print(dependencies)
            array_gene_one = DependencyFeatures.get_indexes_gene(relation["text"], relation["offset1"],
                                                                 relation["word1"])
            array_gene_two = DependencyFeatures.get_indexes_gene(relation["text"], relation["offset2"],
                                                                 relation["word2"])
            wcdd = 0
            wrcd = 0
            wrdd = 0
            dep_root = dependencies[0][2] - 1
            for dependency in dependencies:
                dep_one = dependency[1] - 1
                dep_two = dependency[2] - 1
                wrcd = DependencyFeatures.check(dep_root, dep_one, dep_two, array_gene_one, wrcd)
                wrdd = DependencyFeatures.check(dep_root, dep_one, dep_two, array_gene_two, wrdd)
                if (dep_one in array_gene_one and dep_two in array_gene_two) or \
                        (dep_one in array_gene_two and dep_two in array_gene_one):
                    wcdd = 1
            dep_feats["WCDD"].append(wcdd)
            dep_feats["WRCD"].append(wrcd)
            dep_feats["WRDD"].append(wrdd)

        return dep_feats
