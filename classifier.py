import nltk
import json
import pandas as pd
import numpy as np
import sys

from sklearn.cross_validation import train_test_split, cross_val_score
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import classification_report, accuracy_score, make_scorer
from sklearn.pipeline import Pipeline, FeatureUnion
from sklearn.externals import joblib
from sklearn import svm

from parsing import parse_command_line, check_file_exists
from transformers.vec_repr_features import Word2VecFeature
from transformers.inter_features import InterSentenceFeatures
from transformers.lexical_features import LexicalFeatures
from transformers.dep_features import DependencyFeatures
from transformers.base_features import BaseFeatures


# Example command
#
#classifier.py --src-train-texts biocreative6_dataset/data/gene_relations_in_sentences.json
# --text-encoding utf-8 --features True --laplace True -o folder_for_model --in-sentences True
#

def get_custom_pipeline():
    if not hand_features:

        print(">>Generate Pipeline without hand-crafted features...")
        return Pipeline([
            ('features', FeatureUnion([
                ('base_feats', BaseFeatures()),
            ])),
            ('tfidf', TfidfTransformer(smooth_idf=laplace)),
           ('clf',LogisticRegression()),
        ])
    if in_sent:
        print(">>Generate Pipeline with hand-crafted features in sentences...")
        part_of_filename = filename.split("/")

        # Load dependencies dataset
        with open(
                'data/data_for_dependencies/dependency_%s' % part_of_filename[len(part_of_filename) - 1]) as json_data:
            dependencies_dataset = json.load(json_data)

        return Pipeline([
            ('features', FeatureUnion([
                ('word2vec', Word2VecFeature()),
                ('lex_features', LexicalFeatures()),
                ('dep_feats', DependencyFeatures(dependencies_dataset)),
            ])),
            ('tfidf', TfidfTransformer(smooth_idf=laplace)),
            ('clf', LogisticRegression())
        ])

    print(">>Generate Pipeline with hand-crafted features inter sentences...")
    return Pipeline([
        ('features', FeatureUnion([
            ('inter_feats', InterSentenceFeatures()),
        ])),
        ('tfidf', TfidfTransformer(smooth_idf=laplace)),
        ('clf', LogisticRegression())
    ])

def classification_report_with_accuracy_score(y_true, y_pred):
    print(classification_report(y_true, y_pred))
    return accuracy_score(y_true, y_pred) 


if __name__ == "__main__":
    # -----------------------------------------------------------------------
    # READ PARAMETERS IN COMMAND LINE
    # -----------------------------------------------------------------------

    list_parameters = ["--src-train-texts", "--text-encoding", "--features", "--laplace", "-o", "--in-sentences"]
    cust_dict = parse_command_line(sys.argv[1:], list_parameters)
    filename = check_file_exists(cust_dict["--src-train-texts"])
    directory_for_model = check_file_exists(cust_dict["-o"])
    encoding = cust_dict["--text-encoding"]
    convert_to_bool = lambda x: True if x == "True" else False
    hand_features = convert_to_bool(cust_dict["--features"])
    laplace = convert_to_bool(cust_dict["--laplace"])
    in_sent = convert_to_bool(cust_dict["--in-sentences"])

    # -----------------------------------------------------------------------
    # READ DATA
    # -----------------------------------------------------------------------
    print(">>Read data...")
    try:
        with open(filename, encoding=encoding) as json_data:
            d = json.load(json_data)
    except LookupError:
        raise Exception("Невозможно загрузить файл")

    # -----------------------------------------------------------------------
    # PREPOCESSING
    # -----------------------------------------------------------------------
    print(">>Preprocess data...")
    dataset = d["data"]
    # n = 100
    custom_dataset = []
    for elem in dataset:
        for relation in elem["relations"]:
            relation_d = relation
            relation_d['text'] = elem["text"]
            custom_dataset.append(relation_d)
        # if n == 0:
        #     break
        # n -= 1
    custom_dataset = pd.DataFrame(custom_dataset)
    print(custom_dataset[:10])

    # -----------------------------------------------------------------------
    # GENERATE PIPELINE
    # -----------------------------------------------------------------------
    clf = get_custom_pipeline()

    # Training a classifier
#    print(">>Fit classifier...")
#    clf.fit(custom_dataset.drop(columns="rel", axis=1), custom_dataset.rel)

    # -----------------------------------------------------------------------
    # INFORMATIVE FEATURES
    # -----------------------------------------------------------------------


    # -----------------------------------------------------------------------
    # SAVE MODEL
    # -----------------------------------------------------------------------
#    print(">>Save classifier...")
#    joblib.dump(clf, '%s/classifier_%s.pkl' %(directory_for_model, filename.split("/")[-1].split('.')[0]))

    # -----------------------------------------------------------------------
    # CROSS VALIDATION
    # -----------------------------------------------------------------------

    print(">>Calculate Cross-validation score...")
    scores = cross_val_score(clf, custom_dataset.drop(columns="rel", axis=1), custom_dataset.rel, cv=5,
        scoring=make_scorer(classification_report_with_accuracy_score))

    print(">>Accuracy: %0.2f (+/- %0.2f)" % (scores.mean(), scores.std() * 2))
