import json

from stanfordcorenlp import StanfordCoreNLP

if __name__ == "__main__":
    custom_nlp = StanfordCoreNLP('C:\\lib\\stanford-corenlp-full-2018-02-27')
    array = {'../../aimed_dataset/data/relations_in_sentences_train.json' : "dependency_relations_in_sentences_train.json",
             '../../aimed_dataset/data/relations_in_sentences_test.json' : "dependency_relations_in_sentences_test.json",
             '../../biocreative6_dataset/data/gene_relations_in_sentences_train.json' : "dependency_gene_relations_in_sentences_train.json",
             '../../biocreative6_dataset/data/gene_relations_in_sentences_test.json' : "dependency_gene_relations_in_sentences_test.json"}
    for name in array:
        with open(name) as json_data:
            d = json.load(json_data)
        dataset = d["data"]
        f = open(array[name], "w")
        data = []
        for row in dataset:
            dependencies = custom_nlp.dependency_parse(row["text"])
            data.append(dependencies)
        json.dump(data, f)
        f.close()
    custom_nlp.close()
