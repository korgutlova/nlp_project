import warnings

import pandas as pd
import json

import sys
from sklearn.externals import joblib

from parsing import parse_command_line, check_file_exists
from aimed_dataset.help_methods import dump_to_json_file


# Example command
#
# marking_collections.py --lm folder_for_model/classifier.pkl
# --src-texts data/dataset_in_sentences.json
# --o-texts data
#

from transformers.dep_features import DependencyFeatures

if __name__ == "__main__":

    warnings.simplefilter(action='ignore', category=FutureWarning)

    # -----------------------------------------------------------------------
    # READ PARAMETERS IN COMMAND LINE
    # -----------------------------------------------------------------------

    list_parameters = ["--lm", "--src-texts", "--o-texts"]
    cust_dict = parse_command_line(sys.argv[1:], list_parameters)

    # -----------------------------------------------------------------------
    # LOAD TEST DATA
    # -----------------------------------------------------------------------

    print(">>Read data...")
    filename_clf = check_file_exists(cust_dict["--lm"])
    filename_dataset = check_file_exists(cust_dict["--src-texts"])
    out_data_directory = check_file_exists(cust_dict["--o-texts"])

    with open(filename_dataset) as json_data:
        d = json.load(json_data)
    dataset = d["data"]
    custom_dataset = []

    # -----------------------------------------------------------------------
    # PREPOCESSING
    # -----------------------------------------------------------------------

    print(">>Preprocessing...")
    for elem in dataset:
        for relation in elem["relations"]:
            relation_d = relation
            relation_d['text'] = elem["text"]
            custom_dataset.append(relation_d)
    custom_dataset = pd.DataFrame(custom_dataset)
    print(custom_dataset[:10])

    # -----------------------------------------------------------------------
    # LOAD MODEL
    # -----------------------------------------------------------------------
    print(">>Load model...")
    clf = joblib.load(filename_clf)

    part_of_filename = filename_dataset.split("/")

    part_of_filename = part_of_filename[len(part_of_filename) - 1]
    if len(clf.named_steps["features"].get_params()["transformer_list"]) == 3:
        # Load dependencies dataset
        with open(
                'data/data_for_dependencies/dependency_%s' % part_of_filename) as json_data:
            dependencies_dataset = json.load(json_data)
        # print(clf)
        clf.set_params(features=clf.named_steps['features'].set_params(dep_feats=DependencyFeatures(dependencies_dataset)))


    # -----------------------------------------------------------------------
    # PREDICTING
    # -----------------------------------------------------------------------
    print(">>Predict...")
    result = clf.predict(custom_dataset)

    # -----------------------------------------------------------------------
    # MARKING AND SAVE DATASET
    # -----------------------------------------------------------------------
    print(">>Marking and save dataset...")

    i = 0
    for elem in dataset:
        for relation in elem["relations"]:
            relation["rel"] = str(result[i])
            i += 1
    
    dataset = {"data": dataset}
    dump_to_json_file(dataset, out_data_directory + "/marking_" + part_of_filename.split('.')[0])
